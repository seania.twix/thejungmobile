# Installation

> npm install

> ionic serve

# iOS

iOS에서 웹 파싱 라이브러리의 순환 문제가 있어 https://github.com/SeaniaTwix/html-parser 를 사용해야 합니다.
안드로이드나 ionic serve에선 문제 없으니 iOS에서 디버그를 할 게 아니라면 상관 없습니다.