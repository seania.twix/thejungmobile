import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler, Platform } from 'ionic-angular';
import { MyApp } from './app.component';

import { BestPage } from '../pages/best/best';
import { ConfigPage } from '../pages/config/config';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPageModule } from '../pages/login-page/login-page.module';
import { ViewPageModule } from '../pages/view-page/view-page.module';
import { HomePopoverPageModule } from '../pages/home-popover/home-popover.module';

@NgModule({
  declarations: [
    MyApp,
    BestPage,
    ConfigPage,
    HomePage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      Platform: {
        backButtonText: '뒤로',
      }
    }),
    LoginPageModule,
    ViewPageModule,
    HomePopoverPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    BestPage,
    ConfigPage,
    HomePage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
