import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewQuotes } from './view-quotes';

@NgModule({
  declarations: [
    ViewQuotes,
  ],
  imports: [
    IonicPageModule.forChild(ViewQuotes),
  ],
  exports: [
    ViewQuotes
  ]
})
export class ViewQuotesModule {}
