import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePopoverPage } from './home-popover';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@NgModule({
  declarations: [
    HomePopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(HomePopoverPage),
  ],
  exports: [
    HomePopoverPage
  ],
  providers: [
    InAppBrowser
  ]
})
export class HomePopoverPageModule {}
