import { Component, ViewChild, ElementRef } from '@angular/core';

import { IonicPage, NavParams, NavController } from 'ionic-angular';
import { LoginPage } from '../login-page/login-page';

@IonicPage()
@Component({
  templateUrl: 'home-popover.html',
  selector: 'page-homepopover'
})
export class HomePopoverPage {
  goLogin() {
    this.navCtrl.push(LoginPage);
  }

  constructor(private navParams: NavParams, private navCtrl: NavController) {

  }

}
