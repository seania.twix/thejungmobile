import { Component, ViewChild, Input } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { parse } from "romagny13-html-parser";
import { ViewQuotes } from '../view-quotes/view-quotes';


/**
 * Generated class for the ViewPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-view-page',
  templateUrl: 'view-page.html',
})
export class ViewPage {

  @ViewChild('textarea') textarea;

  posts: any[]; /* Array<{
    isRoot: boolean,
    title: string,
    content: string,
    author: string,
    id: number,
    contentData?
  }>;*/

  title: string;
  path: string;
  rootPath = 'https://thejung.net/';
  fullPath = this.rootPath + this.path;

  Renderer: {
    
  };

  request() {
    let req = new XMLHttpRequest;
    this.fullPath = this.rootPath + this.path;

    req.open('POST', this.fullPath, true);
    //req.setRequestHeader('Access-Control-Allow-Origin', this.fullPath);

    req.onreadystatechange = (event) => {
      if (req.readyState === 4) {
        if (req.status === 200) {
          //console.log(req.responseText);

          let parse_13 = parse(req.responseText);
          let node = parse_13[0].children[1].children[0].children[3].children;
          let regex = /<div id="p\w*" class="post has-profile bg\w">/;
          let nodes = [];
          
          node.forEach((elem) => {
            if (elem.name === 'div') {
              if (regex.test(elem.match)) {
                nodes.push(elem);
              }
            }
          });

          //this.getRootPost(parse_13);
          this.storePost(nodes);

          //this.rootPost(parse(req.responseText));
        } else {
          console.log('Error loading page');
        }
      }
    };

    req.send(null);
  }

  quoteThis() {
    this.textarea.setFocus();
  }

  storePost(nodes: RNode[]) {
    function contentDataConverter(content: RNode) {
      function blockquote(blockquoteNode: RNode) {
        let blockquote_author: { name, id };
        let blockquote_content;
        let blockquote_original_id;
        let blockquote_original_date;
        let blockquote_div = blockquoteNode.children[0];
        let blockquote_cite;

        blockquote_div.children.forEach((element) => {
          if (element.name = 'cite') {
            blockquote_cite = element;
          }
        });

        blockquote_author.name = blockquote_cite.children[0].innerHTML;
        blockquote_original_id = blockquote_cite.children[1].attrs[1].value;
        blockquote_original_date = blockquote_cite.children[2].innerHTML;
        blockquote_content.text = blockquote_div.innerHTML;

        return `
          <ion-card id="`+ blockquote_original_id +`">
            <ion-item id="card_header">
              <h3>`+ blockquote_author.name +`씀</h3>
              <h2>`+ blockquote_original_date +`</h2>
            </ion-item>
            <ion-card-content>
              `+ blockquote_content.text +`
            </ion-card-content>
          </ion-card>`
      }

      function blockquoteDefiner(blockquoteNode: RNode) {
        let author: { name, id } = { name: '', id: '' };
        let plain = '';

        author.name = blockquoteNode.children[0].children[0].children[0].innerHTML;
        author.id = blockquoteNode.children[0].children[0].children[0].match.match(/;u=\d*/g)[0].substring(3);

        plain = blockquoteNode.innerHTML.replace(blockquoteNode.children[0].children[0].innerHTML, '').replace('<cite></cite>', '').replace('<div>', '').replace('</div>', '').replace(/\n/g, '').replace(/<br.*>/, '\n');

        return { author: author, plain: plain };
      }

      let tokens = [];
      let result_content: {
        blockquote?: Array<{ author, plain }>,
        youtube?: Array<{ id }>,
        image?: Array<{ path }>,
        plain: string
      } = {
        blockquote: [],
        youtube: [],
        image: [],
        plain: ''
      };

      if (content.children.length > 0) {
        let local_node: RNode;
        content.children.forEach((node) => {
          local_node = node;
          tokens.push(node.name);

        });

        tokens.forEach((token) => {
          if (token == 'blockquote') {
            // there is case of token in 'token'
            if (local_node.children.length >= 1) {
              // check node, node has children more than once
              if (local_node.name == 'blockquote') {
                // this node is blockquote;
                for (let node of local_node.children[0].children) {
                  if (node.name == 'br' || node.name == 'cite') { continue; } // pass if this have expected tag
                  // get node
                  //arr.push(node);
                }
                //console.log(arr);
                result_content.blockquote.push(blockquoteDefiner(local_node));
              }
            }
          }
        });
      }

      // get and set plain

      let unnecessary_elements = [];
      let plain = content.innerHTML;
      for (let node of content.children) {
        if (node.name == 'br' || node.name == 'strong' || node.name == 'em' || node.name == 'span' || node.name == 'img') {
            continue;
        }
        unnecessary_elements.push(node.innerHTML);
      }

      unnecessary_elements.forEach((unnecessary_element) => {
        plain = plain.replace(unnecessary_element, '');
      })

      result_content.plain = plain.replace(/<br.*>/g, '<br />')
        .replace('<blockquote></blockquote>', '')
        .replace('<div class="inline-attachment"></div>', '');


      
      
      return result_content;
    } 

    let title = '';
    let post_id = '';
    let content = '';
    let author = '';

    nodes.forEach((node) => {
      node.children[0].children.forEach((elem) => {
        if (elem.match == '<div class="postbody">') {
          elem.children[0].children.forEach((_node) => {
            if (_node.name == 'h3') {
              title = _node.children[0].innerHTML;
              _node.children[0].match.match(/\d*/g).forEach((id) => {
                if (parseInt(id) >= 0) {
                  post_id = id;
                }
              });
            }
            if (_node.match == '<p class="author">') {
              author = _node.children[1].children[0].children[0].innerHTML;
            }
            if (_node.match == '<div class="content">') {
              //console.log(contentDataConverter(_node));
              //console.log(_node);
              this.posts.push({
                title: title,
                content: contentDataConverter(_node),
                author: author,
                postId: post_id
              });
            }

            
            /*
            try {
              contentDataConverter(content);  
            } catch (err) {
              console.error(content);
            } */
          });
        }
      });
    });


    /*
    for (let node of nodes) {

      let title = node.children[0].children[1].children[0].children[0].children[0].innerHTML;
      let content = node.children[0].children[1].children[0].children[3].innerHTML

      //console.log(htmlTidier(content));

      this.posts.push({title: title, content: htmlTidier(content)});
    } //*/
  }

  viewPostTree(quotes) {
    this.navCtrl.push(ViewQuotes, { quotes: quotes });
  }

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.path = navParams.get('path');
    this.title = navParams.get('title');
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad ViewPage');
    this.posts = [];
    this.request();
  }

}
