import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewPage } from './view-page';

import { Autosize } from 'ionic2-autosize';

@NgModule({
  declarations: [
    ViewPage,
    Autosize
  ],
  imports: [
    IonicPageModule.forChild(ViewPage)
  ],
  exports: [
    ViewPage
  ]
})
export class ViewPageModule {}
