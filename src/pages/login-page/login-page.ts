import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { TabsPage } from '../tabs/tabs';
/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login-page',
  templateUrl: 'login-page.html',
})
export class LoginPage {

  openLogin() {
    let login = this.iab.create('https://thejung.net/ucp.php?mode=login');
    login.show();
  }

  doNotLogin() {
    this.navCtrl.setRoot(TabsPage);
  }

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private iab: InAppBrowser) {
    //
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
