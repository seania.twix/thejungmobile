import { Component } from '@angular/core';
import { NavController, LoadingController, PopoverController } from 'ionic-angular';
import { parse } from "romagny13-html-parser";
import { ViewPage } from '../view-page/view-page';
import { HomePopoverPage } from '../home-popover/home-popover';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  rootPath = 'https://thejung.net/viewforum.php?f=2';
  pageCount = 1;

  lists;
  posts: Array<{title, path, sticky, likes, date, view, replies, author?, tags, last?}>;
  notices: Array<{title, author, path}>;

  infiniteLoadRunning: boolean;

  isEmpty(str) {
    return !(typeof(str) === 'undefined' || !str || 0 === str.length);
  }

  betterName(name: string) {
    function getByteLength(string,byte?,i?,character?){
      for(byte = i = 0; character = string.charCodeAt(i++); byte += character >> 11 ? 3 : character >> 7 ? 2 : 1);
      return byte;
    }

    if (getByteLength(name) >= 19) {
      return name.replace(name.substring(5, name.length), '') + '...';
    } else {
      return name;
    }
  }

  presentPopover(event) {
    let popover = this.popoverCtrl.create(HomePopoverPage);
    popover.present({
      ev: event
    });
  }

  request(url: string, successd?: Function, failed?: Function) {
    let req = new XMLHttpRequest;
    let document = '';

    req.open('POST', url, true);
    //req.setRequestHeader('Access-Control-Allow-Origin', url);

    ///* //
    req.onreadystatechange = (event) => {
      if (req.readyState === 4) {
        if (req.status === 200) {
          console.log('Request Successed. Getting on Documents');
          document = req.responseText;
          let parse_13 = parse(document);
          let body = parse_13[0].children[1];
          let wrap: RNode;
          let postList: RNode;
          let announce_lists: RNode[];
          let lists: RNode[];

          body.children.forEach((body_elem) => {
            if (/<div id="wrap" class="wrap">/.test(body_elem.match)) {
              wrap = body_elem;
            }
          });
          wrap.children.forEach((wrap_elem) => {
            if (/id="page-body"/.test(wrap_elem.match)) {
              postList = wrap_elem;
            }
          });
          postList.children.forEach((post_elem) => {
            if (/<div class="forumbg announcement">/.test(post_elem.match)) {
              announce_lists = post_elem.children[0].children[1].children;
            }
          });
          postList.children.forEach((post_elem) => {
            if (/<div class="forumbg">/.test(post_elem.match)) {
              lists = post_elem.children[0].children[1].children;
            }
          });

          for (let notice of announce_lists) {
            this.getNotice(notice);
          }

          let count = 0;
          for (let list of lists) {
            count++;
            //console.log(list);
            let sticky = /<li class="row bg\d sticky/g;
            let post = /<li class="row bg\d/g;
            if (sticky.test(list.match)) {
              this.getPosts(list, true);
            } else if (post.test(list.match)) {
              this.getPosts(list, false);
            } else {
              if (typeof(failed) == 'function') {
                failed();
              }
            }
          }

          if (typeof(successd) == 'function') {
            successd();
          }
        } else {
          console.log('Error loading page');
          if (typeof(failed) == 'function') {
            failed();
          }
        }
      }
    }; //*/

    req.send(null);
  }

  refresh(refresher) {
    //console.log('Begin async operation', refresher);
    this.posts = [];
    this.notices = [];
    this.pageCount = 1;
    this.request(this.rootPath, () => {
      refresher.complete();
    });
  }

  getNotice(node: RNode) {

    let title;
    let author;
    let path;

    node.children[0].children.forEach((_node) => {
      if (/<div class="list-inner">/g.test(_node.innerHTML)) {
        node.children.forEach((__node) => {
          __node.children.forEach((post) => {
            if (/<div class="list-inner">/g.test(post.innerHTML)) {
              post.children.forEach((_post) => {
                if (_post.match == '<div class="list-inner">') {
                  // expected _post's 0 is a subject.
                  title = _post.children[0].children[0].innerHTML.replace(/\n/g, '').replace(/\t/g, '');
                  author = _post.children[1].children[0].innerHTML.replace(/\n/g, '').replace(/\t/g, '');
                  //viewtopic.php?f=2&amp;t=26624&amp;sid=4364f62afdc279fa2f02f9a4ce02387f
                  path = _post.children[0].children[0].match.match(/\.\/viewtopic.php\?f=\d*&amp;t=\d*/).toString().replace(/&amp;/g, '&');
                }
              }); 
            }
          });
        });
      }
    });


    this.notices.push({ title: title, author: author, path: path });
  }
  

  getPosts(node: RNode, isSticky?: boolean) {
    //console.log(node);
    function decoder(source: string) {
      //let data = 'https://dev.w3.org/html5/html-author/charref'

      let result = source
                         .replace(/&quot;/g, '"')
                         .replace(/&amp;/g, '&')
                         .replace(/&lt;/g, '<')
                         .replace(/&gt;/g, '>')
                         .replace(/&ndash;/g, '–')
                         .replace(/&lsquo;/g, '‘')
                         .replace(/&rsquo;/g, '’')
                         .replace(/&sbquo;/g, '‚')
                         .replace(/&ldquo;/g, '“')
                         .replace(/&rdquo;/g, '”');

      return result;
    }

    let count = 0;
    let title, path, sticky, likes, date, view, replies, author, tags: Array<any>, last;

    let data = node.children[0].children[0].children[0].children;
    console.log(data);

    title = data[0].children[0].innerHTML.replace(/\n/g, '').replace(/\t/g, '');
    path = data[0].children[0].match.match(/\.\/viewtopic.php\?f=\d*&amp;t=\d*/).toString().replace(/&amp;/g, '&');
    author = data[1].children[0].innerHTML.replace(/\n/g, '').replace(/\t/g, '');
    last = data[1].children[1].innerHTML.replace(/\n/g, '').replace(/\t/g, '');
    replies = data[2].children[0].innerHTML.toString().replace(/\n/g, '').replace(/\t/g, '').replace('<i class="icon fa-reply"></i>', '');
    likes = data[2].children[1].innerHTML.toString().replace(/\n/g, '').replace(/\t/g, '').replace('<i class="icon fa-thumbs-up"></i>', '');
    view = data[2].children[2].innerHTML.toString().replace(/\n/g, '').replace(/\t/g, '').replace('<i class="icon fa-eye"></i>', '');
    tags = [];

    if (data[3].children.length > 0) {
      data[3].children.forEach(elem => {
        elem.children.forEach(tag => {
          tags.push(tag.innerHTML);
        });
      });
    }

    this.posts.push({
      title: title,
      path: path,
      author: author,
      last: last,
      replies: replies,
      likes: likes,
      view: view,
      tags: tags,
      sticky: isSticky,
      date: { last: '' }
    });
  }

  view(post) {
    //console.log(post);
    this.navCtrl.push(ViewPage, { path: post.path, title: post.title });
  }

  infinite(infiniteScroll) {
    if (!this.infiniteLoadRunning) {
      this.infiniteLoadRunning = true;
      let loadPath = '&start=';
      let req = new XMLHttpRequest;
      let document = '';
      req.open('POST', this.rootPath + loadPath + this.pageCount * 60, true);

      req.onreadystatechange = (event) => {
        if (req.readyState === 4) {
          if (req.status === 200) {
            console.log('start infinite scroll', this.pageCount * 60);
            document = req.responseText;
            let parse_13 = parse(document);
            let body = parse_13[0].children[1];
            let wrap: RNode;
            let postList: RNode;
            let lists: RNode[];

            body.children.forEach((body_elem) => {
              if (/<div id="wrap" class="wrap">/.test(body_elem.match)) {
                wrap = body_elem;
              }
            });
            wrap.children.forEach((wrap_elem) => {
              if (/id="page-body"/.test(wrap_elem.match)) {
                postList = wrap_elem;
              }
            });
            postList.children.forEach((post_elem) => {
              if (/<div class="forumbg">/.test(post_elem.match)) {
                lists = post_elem.children[0].children[1].children;
              }
            });

            console.log(lists);

            let count = 0;
            for (let list of lists) {
              count++;
              //console.log(list);
              let sticky = /<li class="row bg\d sticky/g;
              let post = /<li class="row bg\d/g;
              if (sticky.test(list.match)) {
                this.getPosts(list, true);
              } else if (post.test(list.match)) {
                this.getPosts(list, false);
              } else {
                ;
              }
            }

            // this code is done.
            this.infiniteLoadRunning = false;
            this.pageCount++;
            infiniteScroll.complete();
            console.log('infinite loading complete');
          } else {
            console.log('Error loading page');
          }
        }
      };

      req.send(null);
    }
  }

  constructor(public navCtrl: NavController,
              private loadingCtrl: LoadingController,
              private popoverCtrl: PopoverController) {
    this.notices = [];
    this.posts = [];
    this.infiniteLoadRunning = false;
  }

  ionViewDidLoad() {
    let loading = this.loadingCtrl.create({
      content: "데이터 로드 중...",
    });

    loading.present().then(() => {
      this.request(this.rootPath, () => {
        loading.dismiss();
      }, null);
    });
  }

}
